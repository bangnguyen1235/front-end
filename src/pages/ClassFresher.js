import { filter } from 'lodash';
import { Icon } from '@iconify/react';
import React, { useEffect, useState } from 'react';
import plusFill from '@iconify/icons-eva/plus-fill';
import axios from 'axios';
import { useNavigate, Link as RouterLink } from 'react-router-dom';
// material
import {
  Card,
  Table,
  Stack,
  Button,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination
} from '@mui/material';

// components
import Page from '../components/Page';

import Scrollbar from '../components/Scrollbar';
import SearchNotFound from '../components/SearchNotFound';
import { UserListHead, UserMoreMenu } from '../components/_dashboard/user';

//
import USERLIST from '../_mocks_/user';

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'className', label: 'Name', alignRight: false },
  { id: 'classCode', label: 'Class Code', alignRight: false },
  { id: 'location', label: 'Location', alignRight: false },
  { id: 'isDone', label: 'Status', alignRight: false },
  { id: 'startDate', label: 'Start', alignRight: false },
  { id: 'endDate', label: 'End', alignRight: false },
  { id: 'nameAdmin1', label: 'Admin', alignRight: false },
  { id: 'nameTrainer1', label: 'Trainer', alignRight: false },
  { id: 'planId', label: 'Plan', alignRight: false },
  { id: 'budget', label: 'Budget', alignRight: false },
  { id: '' }
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(array, (_user) => _user.name.toLowerCase().indexOf(query.toLowerCase()) !== -1);
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function User() {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState('name');
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [listClass, setListClass] = useState([]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleClick = (event, name) => {};

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };
  useEffect(() => {
    let isActive = false;
    if (!isActive) {
      axios.get('https://localhost:5001/api/ClassFresher').then((res) => {
        setListClass(res.data);
      });
    }
    return () => {
      isActive = true;
    };
  }, []);

  const [file, setFile] = useState();

  function handleChange(event) {
    setFile(event.target.files[0]);
  }

  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - USERLIST.length) : 0;

  const filteredUsers = applySortFilter(listClass, getComparator(order, orderBy), filterName);

  const isUserNotFound = filteredUsers.length === 0;
  const [selectedRow, setSelectedRow] = React.useState({});
  const nagative = useNavigate();
  const GotoDetail = (e) => {
    nagative('detail');
  };
  return (
    <Page title="Class Fresher | Minimal-UI">
      <Container>
        <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
          <Typography variant="h4" gutterBottom>
            Class Fresher
          </Typography>
          <Button
            variant="contained"
            component={RouterLink}
            to="import"
            startIcon={<Icon icon={plusFill} />}
          >
            Import class
          </Button>
          <Button
            variant="contained"
            component={RouterLink}
            to="create"
            startIcon={<Icon icon={plusFill} />}
          >
            Create a new class
          </Button>
        </Stack>

        <Card>
          <Scrollbar>
            <TableContainer sx={{ minWidth: 800 }}>
              <Table>
                <UserListHead
                  order={order}
                  orderBy={orderBy}
                  headLabel={TABLE_HEAD}
                  rowCount={listClass.length}
                  numSelected={selected.length}
                  onRequestSort={handleRequestSort}
                />
                <TableBody>
                  {filteredUsers
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row) => {
                      const {
                        className,
                        classCode,
                        location,
                        isDone,
                        startDate,
                        endDate,
                        nameAdmin1,
                        nameTrainer1,
                        planId,
                        budget,
                        id
                      } = row;
                      const isItemSelected = selected.indexOf(className) !== -1;

                      return (
                        <TableRow
                          onClick={() => GotoDetail(row.id)}
                          hover
                          key={classCode}
                          tabIndex={-1}
                          role="checkbox"
                          selected={isItemSelected}
                          aria-checked={isItemSelected}
                          style={{ backgroundColor: nameAdmin1 === '' ? 'greenyellow' : '' }}
                        >
                          <TableCell align="left">{className}</TableCell>
                          <TableCell align="left">{classCode}</TableCell>
                          <TableCell align="left">{location}</TableCell>
                          <TableCell align="left">{isDone ? 'Done' : 'Process'}</TableCell>
                          <TableCell align="left">{startDate.slice(0, 10)}</TableCell>
                          <TableCell align="left">{endDate.slice(0, 10)}</TableCell>
                          <TableCell align="left">{nameAdmin1}</TableCell>
                          <TableCell align="left">{nameTrainer1}</TableCell>
                          <TableCell align="left">{planId}</TableCell>
                          <TableCell align="left">{budget}</TableCell>
                          <TableCell align="right">
                            <UserMoreMenu />
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
                {isUserNotFound && (
                  <TableBody>
                    <TableRow>
                      <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                        <SearchNotFound searchQuery={filterName} />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )}
              </Table>
            </TableContainer>
          </Scrollbar>

          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={listClass.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
    </Page>
  );
}
