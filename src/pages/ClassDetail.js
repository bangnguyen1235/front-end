// components

import { Card, Table, Stack, Button, Grid, ButtonBase } from '@mui/material';
import { useNavigate, Link as RouterLink } from 'react-router-dom';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Page from '../components/Page';
// ----------------------------------------------------------------------

export default function ClassDetail() {
  const [classdetail, setClassDetail] = useState();
  useEffect(() => {
    axios
      .get('https://localhost:5001/api/ClassFresher/47f48ba8-f1f5-4326-9d3e-954e7f4960ff')
      .then((data) => {
        setClassDetail(data.data);
        console.log(data);
      });
  }, []);
  const handleChange = (event) => {
    classdetail(event.target.value);
  };
  const nagative = useNavigate();
  const Goback = () => {
    nagative('classfreshers/');
  };
  return (
    <Page title="Dashboard: Create class | Minimal-UI">
      <Grid container spacing={4}>
        <Grid item xs={12} md={12} lg={12}>
          {classdetail !== undefined ? (
            <Card
              style={{
                alignItems: 'center',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                height: '600px'
              }}
            >
              <h2>Class Fresher</h2>
              <Grid container spacing={2}>
                <Grid item xs={6} sm={3}>
                  <span>Class Code: {classdetail.classCode}</span>
                </Grid>
                <Grid item xs={6} sm={3}>
                  <span>Skill: {classdetail.className}</span>
                </Grid>
                <Grid item xs={6} sm={3}>
                  <span>Location : {classdetail.location}</span>
                </Grid>
                <Grid item xs={6} sm={3}>
                  <span>Status : {!classdetail.isDone ? 'Proccess' : 'Done'}</span>
                </Grid>
                <Grid item xs={6} sm={3} style={{ alignItems: 'flex-start' }}>
                  <span>Plan : {classdetail.planId}</span>
                </Grid>
                <Grid item xs={6} sm={3} style={{ alignItems: 'flex-start' }}>
                  <span>Start Date : {classdetail.startDate}</span>
                </Grid>
                <Grid item xs={6} sm={3} style={{ alignItems: 'flex-start' }}>
                  <span>End Date : {classdetail.endDate}</span>
                </Grid>
                <Grid item xs={6} sm={3} style={{ alignItems: 'flex-start' }}>
                  {' '}
                </Grid>
                <Grid item xs={6} sm={3}>
                  <span>Admin 1 : {classdetail.nameAdmin1}</span>
                </Grid>
                <Grid item xs={6} sm={3}>
                  <span>Email Admin 1 : {classdetail.emailAdmin1}</span>
                </Grid>
                <Grid item xs={6} sm={3}>
                  <span>Admin 2 : {classdetail.nameAdmin2}</span>
                </Grid>
                <Grid item xs={6} sm={3}>
                  <span>Email Admin 2 : {classdetail.emailAdmin2}</span>
                </Grid>
                <Grid item xs={6} sm={3}>
                  <span>Admin 3 : {classdetail.nameAdmin3}</span>
                </Grid>
                <Grid item xs={6} sm={3}>
                  <span>Email Admin 3 : {classdetail.emailAdmin3}</span>
                </Grid>
                <Grid item xs={6} sm={3}>
                  <span>Trainer 1 : {classdetail.nameTrainer1}</span>
                </Grid>
                <Grid item xs={6} sm={3}>
                  <span>Trainer Admin 1 : {classdetail.emailTrainer1}</span>
                </Grid>
                <Grid item xs={6} sm={3}>
                  <span>Trainer 2 : {classdetail.nameTrainer2}</span>
                </Grid>
                <Grid item xs={6} sm={3}>
                  <span>Email Trainer 2 : {classdetail.emailTrainer2}</span>
                </Grid>
                <Grid item xs={6} sm={3}>
                  <span>Trainer 3 : {classdetail.nameTrainer3}</span>
                </Grid>
                <Grid item xs={6} sm={3}>
                  <span>Email Trainer 3 : {classdetail.emailTrainer3}</span>
                </Grid>
              </Grid>
            </Card>
          ) : null}
          <Grid item xs={12} md={12} lg={12}>
            <ButtonBase>Edit</ButtonBase>
          </Grid>
        </Grid>
      </Grid>
    </Page>
  );
}
