import React, { useState } from 'react';
import {
  Button,
  Container,
  Typography,
  Box,
  Grid,
  Card,
  Tab,
  Table,
  TableBody,
  TableRow,
  TableContainer,
  TableCell
} from '@mui/material';
import axois from 'axios';
import { useForm } from 'react-hook-form';

// components
import Page from '../components/Page';

// ----------------------------------------------------------------------

export default function ImportClass() {
  const fromExcel = [
    { classcode: 'HCM22_FR_NET_01', location: 'HCM' },
    { classcode: 'HCM22_FR_NET_02', location: 'HCM' },
    { classcode: 'HCM22_FR_NET_03', location: 'HCM' },
    { classcode: 'HCM22_FR_NET_04', location: 'HCM' },
    { classcode: 'HCM22_FR_NET_05', location: 'HCM' },
    { classcode: 'HCM22_FR_NET_06', location: 'HCM' }
  ];
  const [selectedFile, setSelectedFile] = React.useState(null);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const formData = new FormData();
    formData.append('selectedFile', selectedFile);
    try {
      console.log(formData);
      await axois({
        method: 'post',
        url: 'https://localhost:5001/api/ClassFresher/import',
        data: formData,
        headers: { 'Content-Type': 'multipart/form-data' }
      }).then((data) => console.log(data.data));
    } catch (error) {
      console.log(error);
    }
  };

  const handleFileSelect = (event) => {
    setSelectedFile(event.target.files[0]);
  };
  return (
    <Page title="Dashboard: Import class | Minimal-UI">
      <Grid container spacing={2}>
        <Grid item xs={12} md={12} lg={12}>
          <Card
            style={{
              alignItems: 'center',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              height: '600px'
            }}
          >
            <form onSubmit={handleSubmit}>
              <input type="file" onChange={handleFileSelect} />
              <input type="submit" value="Upload File" />
            </form>
            {/* <Button type="submit">Import File Excel</Button> */}

            {/* <TableContainer sx={{ minWidth: 800 }}>
              <Table>
                <TableBody>
                  {fromExcel.map((row) => {
                    const { classcode, location } = row;
                    return (
                      <TableRow key={row.classcode}>
                        <TableCell>{classcode}</TableCell>
                        <TableCell>{location}</TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </TableContainer> */}
          </Card>
        </Grid>
      </Grid>
    </Page>
  );
}
