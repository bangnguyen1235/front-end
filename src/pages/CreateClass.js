import React, { useState } from 'react';
import { Card, Grid } from '@mui/material';
import ButtonUI from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
// components
import Page from '../components/Page';

// ----------------------------------------------------------------------
const useStyles = makeStyles((theme) => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white
    }
  },
  paper: {
    marginTop: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));
export default function CreateClass() {
  const classes = useStyles();
  const [age, setAge] = React.useState('');
  const handleChange = (event) => {
    setAge(event.target.value);
  };
  return (
    <Page title="Dashboard: Create class | Minimal-UI">
      <Grid container spacing={2}>
        <Grid item xs={12} md={12} lg={12}>
          <Card
            style={{
              alignItems: 'center',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              height: '600px'
            }}
          >
            <h2>Create a new class fresher</h2>
            <Container component="main">
              <CssBaseline />
              <div className={classes.paper}>
                <form className={classes.form} noValidate>
                  <Grid container spacing={2}>
                    <Grid item xs={6} sm={3}>
                      <h4>Class Code</h4>
                      <TextField
                        autoComplete="fname"
                        name="classcode"
                        variant="outlined"
                        required
                        fullWidth
                        id="classcode"
                        autoFocus
                      />
                    </Grid>
                    <Grid item xs={6} sm={3}>
                      <h4>Location</h4>
                      <TextField
                        autoComplete="fname"
                        name="location"
                        variant="outlined"
                        required
                        fullWidth
                        id="location"
                        autoFocus
                      />
                    </Grid>
                    <Grid item xs={6} sm={3}>
                      <h4>Skill</h4>
                      <FormControl
                        style={{ width: '-webkit-fill-available' }}
                        className={classes.margin}
                      >
                        <NativeSelect
                          id="demo-customized-select-native"
                          value={age}
                          onChange={handleChange}
                        >
                          <option aria-label="None" value="" />
                          <option value={10}>Java</option>
                          <option value={20}>React</option>
                          <option value={30}>C#</option>
                          <option value={40}>PHP</option>
                          <option value={50}>Angular</option>
                          <option value={60}>Auto Test</option>
                          <option value={70}>Flutter</option>
                        </NativeSelect>
                      </FormControl>
                    </Grid>
                    <Grid item xs={6} sm={3}>
                      <h4>Year</h4>
                      <FormControl
                        style={{ width: '-webkit-fill-available' }}
                        className={classes.margin}
                      >
                        <NativeSelect
                          id="demo-customized-select-native"
                          value={age}
                          onChange={handleChange}
                        >
                          <option aria-label="None" value="" />
                          <option value={10}>2021</option>
                          <option value={20}>2022</option>
                        </NativeSelect>
                      </FormControl>
                    </Grid>
                    <Grid item xs={6} sm={3}>
                      <h4>Admin 1</h4>
                      <TextField
                        autoComplete="fname"
                        name="admin1"
                        variant="outlined"
                        required
                        fullWidth
                        id="admin1"
                        autoFocus
                      />
                    </Grid>
                    <Grid item xs={6} sm={3}>
                      <h4>Email</h4>
                      <TextField
                        autoComplete="fname"
                        name="emailAdmin1"
                        variant="outlined"
                        required
                        fullWidth
                        id="emailAdmin1"
                        autoFocus
                      />
                    </Grid>
                    <Grid item xs={6} sm={3}>
                      <h4>Trainer 1</h4>
                      <TextField
                        autoComplete="fname"
                        name="trainer1"
                        variant="outlined"
                        required
                        fullWidth
                        id="trainer1"
                        autoFocus
                      />
                    </Grid>
                    <Grid item xs={6} sm={3}>
                      <h4>Email</h4>
                      <TextField
                        autoComplete="fname"
                        name="emailTrainer1"
                        variant="outlined"
                        required
                        fullWidth
                        id="emailTrainer1"
                        autoFocus
                      />
                    </Grid>
                    <Grid item xs={6} sm={3}>
                      <h4>Admin 2</h4>
                      <TextField
                        autoComplete="fname"
                        name="admin2"
                        variant="outlined"
                        required
                        fullWidth
                        id="admin2"
                        autoFocus
                      />
                    </Grid>
                    <Grid item xs={6} sm={3}>
                      <h4>Email</h4>
                      <TextField
                        autoComplete="fname"
                        name="emailAdmin2"
                        variant="outlined"
                        required
                        fullWidth
                        id="emailAdmin2"
                        autoFocus
                      />
                    </Grid>
                    <Grid item xs={6} sm={3}>
                      <h4>Trainer 2</h4>
                      <TextField
                        autoComplete="fname"
                        name="trainer2"
                        variant="outlined"
                        required
                        fullWidth
                        id="trainer2"
                        autoFocus
                      />
                    </Grid>
                    <Grid item xs={6} sm={3}>
                      <h4>Email</h4>
                      <TextField
                        autoComplete="fname"
                        name="emailTrainer2"
                        variant="outlined"
                        required
                        fullWidth
                        id="emailTrainer2"
                        autoFocus
                      />
                    </Grid>
                    <Grid item xs={6} sm={3}>
                      <h4>Admin 3</h4>
                      <TextField
                        autoComplete="fname"
                        name="admin3"
                        variant="outlined"
                        required
                        fullWidth
                        id="admin3"
                        autoFocus
                      />
                    </Grid>
                    <Grid item xs={6} sm={3}>
                      <h4>Email</h4>
                      <TextField
                        autoComplete="fname"
                        name="emailAdmin3"
                        variant="outlined"
                        required
                        fullWidth
                        id="emailAdmin3"
                        autoFocus
                      />
                    </Grid>
                    <Grid item xs={6} sm={3}>
                      <h4>Trainer 3</h4>
                      <TextField
                        autoComplete="fname"
                        name="trainer3"
                        variant="outlined"
                        required
                        fullWidth
                        id="trainer3"
                        autoFocus
                      />
                    </Grid>
                    <Grid item xs={6} sm={3}>
                      <h4>Email</h4>
                      <TextField
                        autoComplete="fname"
                        name="emailTrainer3"
                        variant="outlined"
                        required
                        fullWidth
                        id="emailTrainer3"
                        autoFocus
                      />
                    </Grid>
                    <Grid item xs={6} sm={4}>
                      <h4>Budget</h4>
                      <TextField
                        autoComplete="fname"
                        name="budget"
                        variant="outlined"
                        required
                        fullWidth
                        id="budget"
                        autoFocus
                      />
                    </Grid>
                    <Grid item xs={6} sm={4}>
                      <h4>Plan</h4>
                      <TextField
                        autoComplete="fname"
                        name="plan"
                        variant="outlined"
                        required
                        fullWidth
                        id="plan"
                        autoFocus
                      />
                    </Grid>
                  </Grid>
                  <ButtonUI
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                  >
                    Create
                  </ButtonUI>
                  {/* <Grid container justify="flex-end">
                    <Grid item>
                      <Link href="/" variant="body2">
                        Already have an account? Sign in
                      </Link>
                    </Grid>
                  </Grid> */}
                </form>
              </div>
            </Container>
          </Card>
        </Grid>
      </Grid>
    </Page>
  );
}
